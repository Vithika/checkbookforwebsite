import { Component, OnInit,Injectable } from '@angular/core';
import { moveIn,fallIn,moveInLeft } from '../router.animations';
import{Router}from  '@angular/router';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {database} from'firebase';
import*as firebase from'firebase';
@Injectable()
@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css'],
   animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})

export class DeleteComponent implements OnInit {
 public accountList: Array<any>;

 public userProfile:firebase.database.Reference;
  constructor( public router:Router) {


    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });
          
        return false
      });
    });
      }
    //this.accountList.push(this.userProfile);
    });   
  
  }
 

 

  ngOnInit() {
  


  }
/*delete(id)
{
 this.af.auth.subscribe(user => {
      if(user) {
        this. userProfile = this.af.database.list(`Accounts/${user.uid}`);
      this.userProfile.remove()
       //const userProfile:FirebaseListObservable<any>=af.database.list(`Accounts/${user.uid}`);      
      }
    });
}*/

 
getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }

delete(accountname)
{
this.userProfile.ref.child(`${accountname}`).remove();
}
   del(id)
  {
    this.delete(id);
  //  firebase.database().ref(this.userProfileRef);

}


}
