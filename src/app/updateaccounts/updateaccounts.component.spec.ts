import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateaccountsComponent } from './updateaccounts.component';

describe('UpdateaccountsComponent', () => {
  let component: UpdateaccountsComponent;
  let fixture: ComponentFixture<UpdateaccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateaccountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateaccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
