import { Component, OnInit,Injectable } from '@angular/core';
import { moveIn,fallIn,moveInLeft } from '../router.animations';

import { AngularFire, FirebaseListObservable } from 'angularfire2';
import* as firebase from'firebase';

@Injectable()
@Component({
  selector: 'app-transferdetails',
  templateUrl: './transferdetails.component.html',
  styleUrls: ['./transferdetails.component.css'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class TransferdetailsComponent implements OnInit {
 public accountList: Array<any>;


public date:Date;
   public details:string;
   public amount:number;
 public userProfile:firebase.database.Reference;
  constructor() {

this.date=new Date();
    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });
          
        return false
      });
    });
      }
    //this.accountList.push(this.userProfile);
  });   
  }

  ngOnInit() {
  }


  addtransfer(date:Date,value:string,values:string,details:string,amount:number):firebase.Promise<any>
{
   console.log(date);
   console.log(details);
   console.log(amount);
   console.log(value);
  return this.userProfile.child(values).child('Transfer').child(date.toString()).set({
    date:new Date(date).toString(),
    to:value,
     from:values,
    details: this.details,
    amount: this.amount,
    
  });
 
}
transfer(date:Date,values:string,value:string,details:string,amount:number)
  {
  
    this.addtransfer(date, values,value,details,amount).then(newaccount=>
    {
      //this.navCtrl.pop();
       console.log("transfer="+date,details,amount,values,value);
    //  this._presentToast("Deposits added successfully") ;
       },error=>
    {
      console.log(error);
    
    })
    alert("details added");
   
    
  }


getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }
}
