import { Component, OnInit,Injectable} from '@angular/core';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {database} from'firebase';
import*as firebase from'firebase';
@Injectable()
@Component({
  selector: 'app-createaccount',
  templateUrl: './createaccount.component.html',
  styleUrls: ['./createaccount.component.css'],
   animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class CreateaccountComponent implements OnInit {
// public    userProfile: FirebaseListObservable<any[]>;
public userProfile:firebase.database.Reference;

public accountname: string;
  public openingbalance: number;
  public paybudgetperiod:number;


  constructor( private af: AngularFire,private router: Router) {

 firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);
      }
    });
 //this. itemObservable = af.database.list('accounts');
  }

  

  ngOnInit() {
  }
 

/*getformdata(accountame,openingbalance,paybudgetperiod)
{
  

 let message = {
      accountame:this.accountname ,
    paybudgetPeriod:this.paybudgetperiod,
      openingbalance:this.openingbalance,
    };
     this.af.auth.subscribe(user => {
      if(user) {
        this.userProfile = this.af.database.list(`Accounts/${user.uid}`+'/'+accountame);
      }
    });
 //   this.itemObservable=this.af.database.list('accounts'+'/'+accountame);gi
   //this.itemObservable=this.af.database.list('accounts'+'/');
  
    this.userProfile.push(message);
 //alert("Your account "+ accountame +" has been created");
    this.router.navigateByUrl('/menu');
    
}*/
getformdata(accountame:string,openingbalance:number,paybudgetPeriod:number):firebase.Promise<any>

{
   return this.userProfile.child(this.accountname).set({
      name: this.accountname,
      paybudget: this. paybudgetperiod,
      opbalance: this.openingbalance
    });
 }


  createaccount(accountname :string, payperiodbudget :number, opbalance:number) {

  this.getformdata(accountname,payperiodbudget,opbalance).then( newaccount => {
     // this.navCtrl.pop();
     // this._presentToast("Successfully created account: "+name +new Date().toLocaleString());
    }, error => {
      console.log(error);
    });

}

  
}
  

