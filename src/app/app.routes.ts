import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MembersComponent } from './members/members.component';
import { AuthGuard } from './auth.service';
import { SignupComponent } from './signup/signup.component';
import { EmailComponent } from './email/email.component';
import{MenuComponent}from './menu/menu.component';
import { CheckbookComponent } from './checkbook/checkbook.component';
import { AddentryComponent } from './addentry/addentry.component';
import { CreateaccountComponent } from './createaccount/createaccount.component';
import { TransferdetailsComponent } from './transferdetails/transferdetails.component';
import { UserguideComponent } from './userguide/userguide.component';
import { UpdateaccountsComponent } from './updateaccounts/updateaccounts.component';
import { JournalComponent } from './journal/journal.component';
import { BudgetdepositComponent } from './budgetdeposit/budgetdeposit.component';
import { DeleteComponent } from './delete/delete.component';


export const router: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'login-email', component: EmailComponent },
    { path: 'members', component: MembersComponent, canActivate: [AuthGuard] },
    { path:'menu', component:MenuComponent,canActivate:[AuthGuard]},
     { path:'checkbook', component:CheckbookComponent,canActivate:[AuthGuard]},
      { path:'addentry', component:AddentryComponent,canActivate:[AuthGuard]},
      { path:'createaccount', component:CreateaccountComponent,canActivate:[AuthGuard]},
       { path:'transferdetails', component:TransferdetailsComponent,canActivate:[AuthGuard]},
       { path:'updateaccounts', component:UpdateaccountsComponent,canActivate:[AuthGuard]},
        { path:'userguide', component:UserguideComponent,canActivate:[AuthGuard]},
           { path:'journal', component:JournalComponent,canActivate:[AuthGuard]},
         { path:'budgetdeposit', component:BudgetdepositComponent,canActivate:[AuthGuard]},
         {path:'delete',component:DeleteComponent,canActivate:[AuthGuard]},
       
    
    
    

]

export const routes: ModuleWithProviders = RouterModule.forRoot(router);