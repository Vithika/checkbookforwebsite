import { Component, OnInit ,Injectable} from '@angular/core';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {database} from'firebase';
import*as firebase from'firebase';

@Injectable()
@Component({
  selector: 'app-addentry',
  templateUrl: './addentry.component.html',
  styleUrls: ['./addentry.component.css'],
  animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class AddentryComponent implements OnInit {

  public accountList: Array<any>;

 public userProfile:firebase.database.Reference;
  constructor() {


    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });
          
        return false
      });
    });
      }
    //this.accountList.push(this.userProfile);
    });   
  
  }
  ngOnInit() {
  }
 onSubmit(formData)
 {
   
 }

 getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }
}
