import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { moveIn,fallIn,moveInLeft } from '../router.animations';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
   animations: [moveIn(),fallIn(),moveInLeft()],
  host: {'[@moveIn]': ''}
})
export class MenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
