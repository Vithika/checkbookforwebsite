import { Component, OnInit,Injectable ,Output} from '@angular/core';
import { Router } from '@angular/router';
import { moveIn, fallIn } from '../router.animations';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {database} from'firebase';
import*as firebase from'firebase';
@Injectable()
@Component({
  selector: 'app-budgetdeposit',
  templateUrl: './budgetdeposit.component.html',
  styleUrls: ['./budgetdeposit.component.css'],
  animations: [moveIn(), fallIn()],
  host: {'[@moveIn]': ''}
})
export class BudgetdepositComponent implements OnInit {


public accountname:string;
public date:Date;
public amount:number;
public details:string;

public selectedAccount:Object={};
 public accountList: Array<any>;


 public userProfile:firebase.database.Reference;
  constructor() {
 

    this.date=new Date();
 
 firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfile = firebase.database().ref(`userProfile/${user.uid}`); 
        console.log(this.userProfile);

        this.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });
          
        return false
      });
    });
      }
    //this.accountList.push(this.userProfile);
    });   
  
  }

 
 adddeposit(date:Date,details:string,amount:number,value:string):firebase.Promise<any>
{
   console.log(date);
   console.log(details);console.log(amount);
   console.log(value);
  return this.userProfile.child(value).child('Deposits').child(date.toString()).set({
    date:new Date(date).toString(),
    details: this.details,
    amount: this.amount,
    name:value
    
  });
 
}
depositbudget(date:Date,details:string,amount:number,value:string)
  {
  
    this.adddeposit(date,details,amount,value).then(newaccount=>
    {
      //this.navCtrl.pop();
       console.log("deposit="+date,details,amount);
    //  this._presentToast("Deposits added successfully") ;
       },error=>
    {
      console.log(error);
    
    })
    alert("details added");
   
    
  }

    
  

 getAccountList(): firebase.database.Reference {
 console.log(this.userProfile);
    return this.userProfile;
   
  }


  ngOnInit() {
  }




}
